
jQuery(function() {
    jQuery('.mobile-nav-open').on('click', function(e) {
        e.preventDefault();
        jQuery('.mobile-nav').slideToggle(300);
        jQuery(this).toggleClass('open');
        jQuery('.mobile-nav-container').toggleClass('open');
    });

    jQuery('.top-search-open a').on('click', function(e) {
        e.preventDefault();
        jQuery('#search_form').fadeToggle();
       
    });

    jQuery('li.level0 > span').on('click', function(e) {
        e.preventDefault();
        var el = jQuery(this);
        if (el.parent().hasClass('menu-open')) {
            el.parent().removeClass('menu-open');
            el.siblings('ul').slideUp();
        }
        else {
            jQuery('li.level0').removeClass('menu-open');
            jQuery('ul.level0').slideUp();
            el.parent().addClass('menu-open');
            el.siblings('ul').slideDown();
        }
    });
});


jQuery(document).ready(function(){
	//jQuery(".damage_claim_form >div:first-child input").focus();
	//alert('load in');
	jQuery('.damage_claim_form > div').each(function(i) {
	if( i == 0 ){
		jQuery(this).find('input').focus();
	}
	});
	
	jQuery(".add-to-box .qty-row input#qty").val("1");
	
	
		jQuery('.home-featured .js-carousel').bxSlider({
			moveSlides:1,
			maxSlides:4,
			slideWidth: 230,
			slideMargin: 10,
			pager:false,
		});
});

			
jQuery(window).on("load resize",function(e){
    var width = jQuery(window).width();
    if (width <= 1100) {
				jQuery('.home-featured .js-carousel').bxSlider({
					moveSlides:1,
				maxSlides:3,
				slideWidth: 270,
				slideMargin: 10
			});
    }
	else if (width <= 850) {
				jQuery('.home-featured .js-carousel').bxSlider({
				moveSlides:1,
				maxSlides:2,
				slideWidth: 300,
				slideMargin: 10
			});
    }
    else if (width <= 650) {
				jQuery('.home-featured .js-carousel').bxSlider({
				moveSlides:1,
				maxSlides:1,
				slideWidth: 300,
				slideMargin: 10
			});
    }
});

 

jQuery(document).ready(function() {
	 /*Scroll to top when arrow up clicked BEGIN*/
jQuery(window).scroll(function() {
    var height = jQuery(window).scrollTop();
    if ( height > 100 ) {
        jQuery('#back2Top').fadeIn();
        jQuery('.scroll_top').fadeIn();
    } else {
        jQuery('#back2Top').fadeOut();
        jQuery('.scroll_top').fadeOut();
    }
});
    jQuery("#back2Top").on('click', function(event) {
   
        jQuery("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
 /*Scroll to top when arrow up clicked END*/


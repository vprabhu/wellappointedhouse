jQuery.noConflict();
jQuery(document).ready(function($) {
    var $changer_input = $('.js-select-input'),
        $selects = $('.main-container select');

    $selects.each(function() {
        var $self = $(this),
            my_id = $self.attr('id'),
            $my_select;

        $self.addClass('js-is-styled is-styled');

        if (my_id && my_id.indexOf(':') !== -1) {
            $self.attr('id', my_id.replace(':', '-'));
        }
        $self.selectmenu({
            width: $self.outerWidth(true),
            change: function(e, ui) {
                if ("createEvent" in document) {
                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", false, true);
                    e.target.dispatchEvent(evt);
                }
                else {
                    e.target.fireEvent("onchange");
                }

                $selects.selectmenu('refresh');

                $changer_input.each(function() {
                    var $self = $(this),
                        $my_select = $self.siblings('.ui-selectmenu-button');

                    if ($self.is(':visible') && $my_select.is(':visible')) {
                        $my_select.hide();
                    } else if ($self.is(':hidden') && $my_select.is(':hidden')) {
                        $my_select.show();
                    }
                });
            }
        });
        if (my_id && my_id.indexOf(':') !== -1) {
            $self.attr('id', my_id);
        }
        if ($self.hasClass('js-select-hidden')) {
            $self.addClass('select-hidden');
        }
        $self.siblings('.ui-selectmenu-disabled').removeClass('ui-selectmenu-disabled ui-state-disabled');

        $changer_input.each(function() {
            var $self = $(this),
                $my_select = $self.siblings('.ui-selectmenu-button');

            if ($self.is(':visible') && $my_select.is(':visible')) {
                $my_select.hide();
            } else if ($self.is(':hidden') && $my_select.is(':hidden')) {
                $my_select.show();
            }
        });

        $self.removeAttr('style');
    });

    if (!$('.logo img').attr('width')) {
        $('.logo img').attr({
            'width': '362',
            'height': '139'
        });
    }
    if (!$('.top-link-cart img').attr('width')) {
        $('.top-link-cart img').attr({
            'width': '23',
            'height': '28'
        });
    }
});
function styleSelects() {
    jQuery('.main-container select').each(function() {
        var $self = jQuery(this),
            my_id = $self.attr('id');

        if ($self.hasClass('js-is-styled')) {
            return;
        }

        $self.addClass('js-is-styled is-styled');

        if (my_id && my_id.indexOf(':') !== -1) {
            $self.attr('id', my_id.replace(':', '-'));
        }
        $self.selectmenu({
            disable: false,
            width: $self.outerWidth(true),
            change: function(e) {
                if ("createEvent" in document) {
                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", false, true);
                    e.target.dispatchEvent(evt);
                }
                else {
                    e.target.fireEvent("onchange");
                }
            }
        });
        if (my_id && my_id.indexOf(':') !== -1) {
            $self.attr('id', my_id);
        }
        if ($self.hasClass('js-select-hidden')) {
            $self.addClass('select-hidden');
        }
        $self.siblings('.ui-selectmenu-disabled').removeClass('ui-selectmenu-disabled ui-state-disabled');

        jQuery('.js-select-input').each(function() {
            var $self = jQuery(this),
                $my_select = $self.siblings('.ui-selectmenu-button');

            if ($self.is(':visible') && $my_select.is(':visible')) {
                $my_select.hide();
            } else if ($self.is(':hidden') && $my_select.is(':hidden')) {
                $my_select.show();
            }
        });

        $self.removeAttr('style');
    });
}
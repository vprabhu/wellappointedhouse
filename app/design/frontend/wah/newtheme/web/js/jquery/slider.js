/**
 * Created by Savich on 5/8/2014.
 */


jQuery(document).ready(function(){

    //jQuery('#pricesliderisloading').hide();

    function makeSeparator(val) {
        var sep = ',',
            string_number = val + '',
            max = string_number.length,
            result = '',
            counter = 0;

        for (; max--;) {
            if (string_number.charAt(max) !== '') {
                if (counter < 3) {
                    result = string_number.charAt(max) + result;
                    counter++;
                } else {
                    result = string_number.charAt(max) + sep + result;
                    counter = 0;
                }
            }
        }

        return result;
    }

    var min = parseInt(jQuery('#minprice').val());
    var max = parseInt(jQuery('#maxprice').val());

    var clean_url = jQuery('#cleanurl').val();

    var current_min = min,
        current_max = max,
        $min_wr = jQuery('#showcurrentmin'),
        $max_wr = jQuery('#showcurrentmax'),
        $min_wr_par = false,
        $max_wr_par = false,
        $priceslider = jQuery("#priceslider"),
        $priceslider_width = false;

    $min_wr.html('$' + makeSeparator(current_min));
    $max_wr.html('$' + makeSeparator(current_max));

    $priceslider.slider({
        range: true,
        min: current_min,
        max: current_max,
        values:[current_min,current_max],
        slide: function( event, ui ) {
            if (!$min_wr_par) {
                $min_wr_par = $min_wr.parent();
            }
            if (!$max_wr_par) {
                $max_wr_par = $max_wr.parent();
            }

            if ($min_wr_par.position().left > $min_wr.width() + 10) {
                $min_wr_par.addClass('revert');
            } else {
                $min_wr_par.removeClass('revert');
            }
            if ($max_wr_par.position().left < $priceslider_width - $max_wr.width() + 10) {
                $max_wr_par.addClass('revert');
            } else {
                $max_wr_par.removeClass('revert');
            }
            current_min = ui.values[ 0 ];
            current_max = ui.values[ 1 ];

            $min_wr.html('$' + makeSeparator(current_min));
            $max_wr.html('$' + makeSeparator(current_max));
        } ,
        stop: function(event, ui){

            var clue_char = '?';
            if( clean_url.indexOf("?") >= 0 ){
                clue_char = "&";
            }

            window.location.href = clean_url + clue_char + 'price=' + current_min + '-' + (current_max + 0.01);
        }
    });
    $priceslider_width = $priceslider.width();
    $priceslider.find('.ui-slider-handle').eq(0).append($min_wr);
    $priceslider.find('.ui-slider-handle').eq(1).append($max_wr);

});

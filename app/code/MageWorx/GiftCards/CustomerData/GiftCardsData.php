<?php
/**
 * Copyright © 2018 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\GiftCards\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

class GiftCardsData implements SectionSourceInterface
{
    /**
     * var@ \MageWorx\GiftCards\Model\Session
     */
    protected $giftCardSession;


    /**
     * @param \MageWorx\GiftCards\Model\Session $giftCardSession
     */
    public function __construct(
        \MageWorx\GiftCards\Model\Session $giftCardSession
    ) {
        $this->giftCardSession = $giftCardSession;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $applied = 0.0;

        $frontOptions = $this->giftCardSession->getFrontOptions();
        if ($frontOptions && is_array($frontOptions)) {
            foreach ($frontOptions as $key => $option) {
                $applied += (real)$option['applied'];
            }
        }
        return [
            'mageworx_giftcards_amount' => $applied,
        ];
    }

}

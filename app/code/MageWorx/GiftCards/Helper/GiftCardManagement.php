<?php
/**
 * Copyright © 2018 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\GiftCards\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use MageWorx\GiftCards\Model\Session as GiftCardSession;
use MageWorx\GiftCards\Api\Data\GiftCardsInterface;
use Magento\Framework\App\Helper\Context as ContextHelper;

class GiftCardManagement extends AbstractHelper
{
    /**
     * @var GiftCardSession
     */
    private $giftCardSession;

    /**
     * GiftCardManagement constructor.
     * @param ContextHelper $context
     * @param GiftCardSession $giftCardSession
     */
    public function __construct(
        ContextHelper $context,
        GiftCardSession $giftCardSession
    ) {
        $this->giftCardSession = $giftCardSession;
        parent::__construct($context);
    }

    /**
     * @param GiftCardsInterface $giftCard
     */
    public function setSessionVars(GiftCardsInterface $giftCard)
    {
        $giftCardsIds = $this->giftCardSession->getGiftCardsIds();

        if (!empty($giftCardsIds)) {
            if (!array_key_exists($giftCard->getId(), $giftCardsIds)) {
                $giftCardsIds[$giftCard->getId()] = [
                    'balance' => $giftCard->getCardBalance(),
                    'code' => $giftCard->getCardCode()
                ];
                $this->giftCardSession->setGiftCardsIds($giftCardsIds);

                $newBalance = $this->giftCardSession->getGiftCardBalance() + $giftCard->getCardBalance();
                $this->giftCardSession->setGiftCardBalance($newBalance);
            }
        } else {
            $giftCardsIds[$giftCard->getId()] = [
                'balance' => $giftCard->getCardBalance(),
                'code' => $giftCard->getCardCode()
            ];

            $this->giftCardSession->setGiftCardsIds($giftCardsIds);

            $this->giftCardSession->setGiftCardBalance($giftCard->getCardBalance());
        }
    }
}

<?php
/**
 * Copyright © 2017 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\GiftCards\Helper;

/**
 * MageWorx data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**#@+
     * XML paths for config settings
     */
    const XML_SHOW_IN_CART = 'mageworx_giftcards/main/show_in_shopping_cart';
    const XML_CARD_ACTIVATION_ORDER_STATUSES = 'mageworx_giftcards/main/orderstatus';
    const XML_ORDER_STATUSES = 'mageworx_giftcards/email/orderstatus';
    const XML_ADD_CODE_TO_PRODUCT = 'mageworx_giftcards/main/add_code_to_product';
    const XML_SHOW_MAIL_DELIVERY_DATE = 'mageworx_giftcards/main/show_mail_delivery_date';
    const XML_SUPPORT_MAIL = 'trans_email/ident_general/email';
    const XML_DISABLE_MULTISHIPPING = 'mageworx_giftcards/main/disable_multishipping';
    const XML_EXPIRATION_ALERT = 'mageworx_giftcards/email/expiration_alert';
    const XML_EMAIL_TEMPLATE_IDENTIFIER = 'mageworx_giftcards/email/email_template';
    const XML_PRINT_TEMPLATE_IDENTIFIER = 'mageworx_giftcards/email/print_template';
    const XML_OFFLINE_TEMPLATE_IDENTIFIER = 'mageworx_giftcards/email/offline_template';
    const XML_EXPIRED_TEMPLATE_IDENTIFIER = 'mageworx_giftcards/email/expired_template';
    const XML_EXPIRATION_ALERT_TEMPLATE_IDENTIFIER = 'mageworx_giftcards/email/expiration_alert_template';


    /**
     * @var \Magento\Store\Model\Information
     */
    protected $storeInformation;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\Information $storeInformation
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\Information $storeInformation
    ) {
    
        $this->storeInformation = $storeInformation->getStoreInformationObject($storeManager->getStore());
        parent::__construct($context);
    }

    public function showInCart()
    {
        return (bool) $this->scopeConfig->getValue(self::XML_SHOW_IN_CART, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getStoreName()
    {
        return $this->storeInformation['name'];
    }
    
    public function getSupportMail()
    {
        return $this->scopeConfig->getValue(self::XML_SUPPORT_MAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getOrderStatuses()
    {
        return $this->scopeConfig->getValue(self::XML_ORDER_STATUSES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Retrieve comma-separated order statuses
     *
     * @return string|null
     */
    public function getCardActivationOrderStatuses()
    {
        return $this->scopeConfig->getValue(
            self::XML_CARD_ACTIVATION_ORDER_STATUSES,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function getAddCodeToProduct()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_ADD_CODE_TO_PRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getStorePhone()
    {
        return $this->storeInformation['phone'];
    }

    /**
     * @return bool
     */
    public function getShowMailDeliveryDate()
    {
        return (bool) $this->scopeConfig->getValue(self::XML_SHOW_MAIL_DELIVERY_DATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function getIsDisableMultishipping()
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_DISABLE_MULTISHIPPING,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $xmlPath
     * @return mixed
     */
    public function getConfigValue($xmlPath)
    {
        return $this->scopeConfig->getValue($xmlPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $item
     * @return bool
     */
    public function isExpired($item)
    {
        if (!$item->getExpireDate()) {
            return false;
        }
        $daysLeft = $this->calculateExpireIn($item->getExpireDate());
        return $daysLeft < 0;
    }

    /**
     * @param $product
     * @return bool|string
     */
    public function getExpireDateForProduct($product)
    {
        $lifetimeValue = $product->getMageworxGcLifetimeValue();
        $expireDate = '';
        if (!empty($lifetimeValue)) {
            $expireDate = date_add(date_create(), date_interval_create_from_date_string($lifetimeValue . 'days'));
        }

        return $expireDate;
    }

    /**
     * @param string $expireDate
     * @return float
     */
    public function calculateExpireIn($expireDate)
    {
        return round((strtotime($expireDate)) - strtotime(date('M d Y'))) / 3600 / 24;
    }

    /**
     * @return string
     */
    public function getAlertDays()
    {
        return $this->scopeConfig->getValue(self::XML_EXPIRATION_ALERT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * @return string
     */
    public function getEmailTemplateIdentifier()
    {
        return $this->scopeConfig->getValue(self::XML_EMAIL_TEMPLATE_IDENTIFIER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * @return string
     */
    public function getPrintTemplateIdentifier()
    {
        return $this->scopeConfig->getValue(self::XML_PRINT_TEMPLATE_IDENTIFIER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getOfflineTemplateIdentifier()
    {
        return $this->scopeConfig->getValue(self::XML_OFFLINE_TEMPLATE_IDENTIFIER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getExpirationAlertTemplateIdentifier()
    {
        return $this->scopeConfig->getValue(self::XML_EXPIRATION_ALERT_TEMPLATE_IDENTIFIER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getExpiredAlertTemplateIdentifier()
    {
        return $this->scopeConfig->getValue(self::XML_EXPIRED_TEMPLATE_IDENTIFIER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

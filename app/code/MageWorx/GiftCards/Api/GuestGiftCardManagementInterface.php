<?php
/**
 *
 * Copyright © 2018 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\GiftCards\Api;

/**
 * @api
 */
interface GuestGiftCardManagementInterface
{
    /**
     * @param string $cartId
     * @param string $giftCardCode
     * @return boolean
     */
    public function applyToCart($cartId, $giftCardCode);

    /**
     * @param string $cartId
     * @param string $giftCardCode
     * @return boolean
     */
    public function removeFromCart($cartId, $giftCardCode);
}

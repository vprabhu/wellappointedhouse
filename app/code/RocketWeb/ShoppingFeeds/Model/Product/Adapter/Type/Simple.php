<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    Rocket Web Inc.
 */

namespace RocketWeb\ShoppingFeeds\Model\Product\Adapter\Type;

use \RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterAbstract;
use \RocketWeb\ShoppingFeeds\Model\Product\Adapter\AdapterInterface;
/**
 * Simple Adapter, holds business logic between Product,Config and Mapper
 *
 * Class Simple
 * @package RocketWeb\ShoppingFeeds\Model\Product\Adapter
 */
class Simple extends AdapterAbstract implements AdapterInterface
{
}
<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    Rocket Web Inc.
 */
namespace RocketWeb\ShoppingFeeds\Cron;

use RocketWeb\ShoppingFeeds\Model\Feed;
use RocketWeb\ShoppingFeeds\Model\FeedFactory;
use RocketWeb\ShoppingFeeds\Model\Generator;

class Schedule
{
    const XML_PATH_ENABLED = 'shoppingfeeds/general/cron_enabled';

    /**
     * @var Generator\BatchFactory
     */
    protected $batchFactory;

    /**
     * @var int
     */
    protected $counter = 0;

    /**
     * @var FeedFactory
     */
    protected $feedFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \RocketWeb\ShoppingFeeds\Model\ResourceModel\Feed\Schedule\Collection
     */
    protected $scheduleCollection;

    /**
     * @var \RocketWeb\ShoppingFeeds\Model\ResourceModel\Generator\Queue\Collection
     */
    protected $queueCollection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var bool.
     * Is set to you when process is initiated through console and not magento's cron
     */
    protected $detached = false;


    public function __construct(
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \RocketWeb\ShoppingFeeds\Model\Generator\BatchFactory $batchFactory,
        \RocketWeb\ShoppingFeeds\Model\FeedFactory $feedFactory,
        \RocketWeb\ShoppingFeeds\Model\ResourceModel\Feed\Schedule\Collection $scheduleCollection,
        \RocketWeb\ShoppingFeeds\Model\ResourceModel\Generator\Queue\Collection $queueCollection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->batchFactory = $batchFactory;
        $this->feedFactory = $feedFactory;
        $this->localeDate = $localeDate;
        $this->scheduleCollection = $scheduleCollection;
        $this->queueCollection = $queueCollection;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) ($this->scopeConfig->getValue(self::XML_PATH_ENABLED)) || $this->detached;
    }

    /**
     * Add feeds which should be generated to queue
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->isEnabled()) return;

        /** @var \DateTime $dateObject */
        $dateObject = $this->localeDate->date();
        $yesterday = clone $dateObject;
        $yesterday->setTime(0,0);
        $dateTimeFormat = \Magento\Framework\DB\Adapter\Pdo\Mysql::DATETIME_FORMAT;

        $this->scheduleCollection->setHourFilter($dateObject->format('H'))
            ->setDateFilter($yesterday->format($dateTimeFormat));

        /** @var \RocketWeb\ShoppingFeeds\Model\Feed\Schedule $schedule */
        foreach ($this->scheduleCollection as $schedule) {
            $queue = $this->queueCollection->getQueue($schedule->getFeedId());
            if (!$queue->getId()) {
                $feed = $this->feedFactory->create()
                    ->load($schedule->getFeedId());
                $queue->setFeedId($schedule->getFeedId());
                if ($schedule->getBatchMode()) {
                    $batch = $this->batchFactory->create();
                    $batch->setEnabled(true)
                        ->setLimit($schedule->getBatchLimit())
                        ->setOffset(0);
                    $queue->setBatch($batch);
                }
                // Add new queue for process
                $queue->save();
                $feed->saveStatus(\RocketWeb\ShoppingFeeds\Model\Feed\Source\Status::STATUS_PENDING);
                $schedule->setProcessedAt($dateObject->format($dateTimeFormat));
                $schedule->save();
                $this->counter++;
            }
        }
    }

    public function getCounter()
    {
        return $this->counter;
    }

    public function setDetached()
    {
        $this->detached = true;
        return $this;
    }
}

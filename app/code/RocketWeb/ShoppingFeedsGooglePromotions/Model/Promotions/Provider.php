<?php

namespace RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions;

class Provider
{
    const MAX_PROMOTION_DAYS = 183;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Quote\Model\Quote\ItemFactory
     */
    protected $quoteItem;

    /**
     * @var Provider\Map
     */
    protected $map;

    /**
     * @var Provider\Collection
     */
    protected $promotionsCollection;

    /**
     * @var \RocketWeb\ShoppingFeeds\Model\Feed
     */
    private $feed = null;

    /**
     * @var string
     */
    protected $hashCache = false;

    /**
     * @var bool
     */
    protected $fileCreated = false;


    public function __construct(
        \Magento\Framework\Json\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItem,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Map $map,
        \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Collection $promotionsCollection
    )
    {
        $this->helper = $helper;
        $this->directoryList = $directoryList;
        $this->storeManager = $storeManager;
        $this->quoteItem = $quoteItem;
        $this->objectManager = $objectManager;
        $this->map = $map;
        $this->promotionsCollection = $promotionsCollection;
    }

    /**
     * @param \RocketWeb\ShoppingFeeds\Model\Feed $feed
     * @return $this
     */
    public function setFeed(\RocketWeb\ShoppingFeeds\Model\Feed $feed)
    {
        $this->feed = $feed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return 'Y/m/d';
    }

    /**
     * @return string
     */
    public function getPromotionDateFormat()
    {
        return 'Y-m-d';
    }

    /**
     * @return bool
     */
    public function isFileCreated()
    {
        return $this->fileCreated;
    }

    /**
     * @return $this
     */
    public function setFileCreated()
    {
        $this->fileCreated = true;
        return $this;
    }

    /**
     * @return string
     */
    public function getPromotionFile()
    {
        $feed= $this->getFeed();
        return $this->directoryList->getRoot(). '/'
            . rtrim($feed->getConfig('general_feed_dir'), '/'). '/'
            . sprintf($feed->getConfig('file_promotion'), $feed->getId());
    }

    /**
     * Compare magento collection & saved data collection - if saved row doesn't exists
     * in magento collection, it should be removed!
     *
     * @param \RocketWeb\ShoppingFeeds\Model\Feed $feed
     * @return $this
     */
    public function validateGooglePromotions()
    {
        $config = $this->getFeed()->getConfig('promotions_provider_widget');
        $cartRulesIds = $this->promotionsCollection->getPromotionRules($this->getFeed())->getAllIds();

        if (isset($config['promotion'])) {
            $promotionIds = array_keys($config['promotion']);
            $removeIds = array_diff($promotionIds, $cartRulesIds);
            if (count($removeIds) > 0) {
                foreach ($removeIds as $removeId) {
                    unset($config['promotion'][$removeId]);
                }
                $this->promotionsCollection->updatePromotionsData($this->getFeed(), $config);
            }
        }
        return $this;
    }

    /**
     * Getter method to handle cache
     *
     * @param int $productId
     * @param string $hash
     * @return bool
     */
    protected function getPromotionCache($productId = 0, $hash = '')
    {
        $cache = $this->getHashCache($hash);
        if (isset($cache[$productId]) && is_array($cache[$productId])) {
            return $cache[$productId];
        }
        return false;
    }

    /**
     * Setter method which also writes to file cache
     * This is used so we have a constant cache over all the tests & shopping feed generations
     *
     * @param int $productId
     * @param string $hash
     * @param array $promotionIds
     * @return $this
     */
    protected function setPromotionCache($productId = 0, $hash = '', $promotionIds = array())
    {
        $cache = $this->getHashCache($hash);
        $cache[$productId] = $promotionIds;

        // Update current cache
        $this->hashCache = $cache;

        //Update file cache
        $cacheFile = $this->getHashFile();
        $fileContent = array(
            'cache' => $cache,
            'hash' => $hash
        );
        @file_put_contents($cacheFile, $this->helper->jsonEncode($fileContent));

        return $this;
    }

    /**
     * Removes cache file
     * Not used currently (it should be in the observer)
     *
     * @return $this
     */
    public function clearPromotionCache()
    {
        $cacheFile = $this->getHashFile();
        @unlink($cacheFile);
        return $this;
    }

    /**
     * Gets cache from file if its not available inside the Varien_Object
     *
     * @param string $hash
     * @return array
     */
    protected function getHashCache($hash = '')
    {
        if (!$this->hashCache) {
            $cacheFile = $this->getHashFile();
            $cache = array();
            if (file_exists($cacheFile) && is_readable($cacheFile)) {
                $fileContent = file_get_contents($cacheFile);
                $tmpCache = $this->helper->jsonDecode($fileContent);
                if (is_array($tmpCache) && isset($tmpCache['hash']) && $tmpCache['hash'] == $hash && isset($tmpCache['cache'])) {
                    $cache = $tmpCache['cache'];
                }
            }
            $this->hashCache = $cache;
        }
        return $this->hashCache;
    }

    /**
     * Returns promotion cache file location
     *
     * @return string
     */
    /**
     * @return string
     */
    protected function getHashFile()
    {
        return rtrim($this->directoryList->getPath('var'), '/') .'/cache/promotions.cache';
    }

    /**
     * Prepare date for output
     *
     * @param \RocketWeb\ShoppingFeeds\Model\Feed $feed
     * @param $promotionDate
     * @param $rowDate
     * @param bool $addDays
     * @param bool $format
     * @return string
     */
    public function prepareDate($promotionDate, $rowDate, $addDaysFrom = false, $format = false)
    {
        /** @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneObject */
        $timezoneObject = $this->objectManager->create('\Magento\Framework\Stdlib\DateTime\Timezone');
        $store = $this->storeManager->getStore($this->getFeed()->getStoreId());

        $timezone = $timezoneObject->getConfigTimezone(\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
        $timezone = $timezone ? new \DateTimeZone($timezone) : new \DateTimeZone(\DateTimeZone::UTC);

        $add = true;
        $date = $timezoneObject->date();

        if (!empty($rowDate)) {
            //$date = $timezoneObject->date($rowDate);
            $date =  $date->createFromFormat($this->getDateFormat(), $rowDate, $timezone);
            $add = false;
        } else if (!empty($promotionDate)) {
            // $date = $timezoneObject->date($promotionDate);
            $date = $date->createFromFormat($this->getPromotionDateFormat(), $promotionDate, $timezone);
            $add = false;
        }
        if (!$date) {
            return '';
        }
        $date->setTime(12, 0, 0);

        if ($addDaysFrom !== false && $add === true) {
            $date = $date->createFromFormat($this->getDateFormat(), $addDaysFrom, $timezone);
            $date->add(new \DateInterval('P'.self::MAX_PROMOTION_DAYS. 'D'));
        }

        //$date->setTimezone($timezone);

        if ($format === false) {
            $format = $this->getDateFormat();
        }

        return $date->format($format);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getPromotionIds(\Magento\Catalog\Model\Product $product)
    {
        $ids = array();
        $productId = $product->getId();
        if (is_null($this->getFeed())) {
            return $ids;
        }
        $config = $this->getFeed()->getConfig('promotions_provider_widget');

        /**
         * If promotion is not correctly structured, we don't go any further
         * If hash is not set, the promotions weren't generated
         */
        if (
            $this->getFeed()->getConfig('promotions_enabled')
            && is_array($config)
            && isset($config['hash'])
            && isset($config['promotion'])
            && count($config['promotion']) > 0
        ) {
            $ids = $this->getPromotionCache($productId, $config['hash']);
            if ($ids === false) {
                $ids = array();
                $this->map->setProvider($this);

                /** @var \Magento\SalesRule\Model\ResourceModel\Rule\Quote\Collection $cartRulesCollection */
                $cartRulesCollection = $this->promotionsCollection->getPromotionRules($this->getFeed());

                /** @var \Magento\SalesRule\Model\Rule $rule */
                foreach ($cartRulesCollection as $rule) {
                    /**
                     * Because the rule->validate works on different types (quote/quote_item/...)
                     * the following manipulation is needed
                     */
                    $quoteItem = $this->quoteItem->create()->setProduct($product);
                    if ($rule->getActions()->validate($quoteItem)) {
                        $ids[] = $this->map->mapPromotionId($config['counter'], $rule);
                    }
                }

                // Save to cache
                $this->setPromotionCache($productId, $config['hash'], $ids);
            }
        }

        return $ids;
    }
}
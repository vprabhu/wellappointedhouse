#### Contents
*   <a href="#over">Overview</a>
*   <a href="#tests">Tests</a>
*   <a href="#lic">License</a>

<h2 id="over">Overview</h2>

This module allows to generate feeds of different types including Google Shopping and Google Promotions feeds.

<h2 id="tests">Tests</h2>

Integration and unit tests can be found in the [Test](Test) directory.

<h2 id="contrib">Author</h2>

Rocket Web Inc. team

<h2 id="lic">License</h2>

[Open Source License](LICENSE.txt)
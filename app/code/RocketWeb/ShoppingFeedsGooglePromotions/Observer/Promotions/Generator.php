<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    Rocket Web Inc.
 */

namespace RocketWeb\ShoppingFeedsGooglePromotions\Observer\Promotions;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class Generator implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    protected $ruleFactory;

    /**
     * @var Provider
     */
    protected $provider;

    /**
     * @var \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Map
     */
    protected $map;

    /**
     * @var \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Collection
     */
    protected $promotionsCollection;


    public function __construct(
        \Magento\Framework\Json\Helper\Data $helper,
        \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider $provider,
        \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Map $map,
        \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Collection $promotionsCollection,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    )
    {
        $this->helper = $helper;
        $this->provider = $provider;
        $this->map = $map;
        $this->ruleFactory = $ruleFactory;
        $this->directoryList = $directoryList;
        $this->promotionsCollection = $promotionsCollection;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        /** @var /RocketWeb/ShoppingFeeds/Model/Generator $generator */
        $generator = $observer->getEvent()->getData('generator');
        if ($generator->isTestMode()) {
            return $this;
        }

        $feed = $observer->getEvent()->getData('feed');
        $generator->getLogger()->info(sprintf('START GOOGLE_PROMOTIONS FEED #%s', $feed->getId()));
        if (!$feed->getConfig('promotions_enabled')) {
            $generator->getLogger()->info('Promotions disabled!');
            return $this;
        }

        // Adjust promotions data to cart rules data.
        $this->provider->setFeed($feed);
        $this->provider->validateGooglePromotions();

        $config = $feed->getConfig('promotions_provider_widget');
        $promotion = $config['promotion'];
        $hashString = $config['counter']. $this->helper->jsonEncode($promotion);
        $hash = md5($hashString);
        $file = $this->provider->getPromotionFile();

        $fileLines = array();
        if (!isset($config['hash']) || $config['hash'] != $hash || !file_exists($file)) {
            $this->provider->clearPromotionCache();
            $this->map->setProvider($this->provider);

            $fileLines[] = $this->createFeedHeader();
            foreach ($promotion as $key => $row) {
                if (array_key_exists('include', $row) && $row['include']) {
                    /** @var \Magento\SalesRule\Model\Rule $rule */
                    $rule = $this->ruleFactory->create()->load($key);
                    $fileLines[] = $this->createFeedLine($config['counter'], $rule, $row, $this->map);
                }
            }
        }

        $fileOutput = '';
        foreach ($fileLines as $line) {
            $fileOutput .= implode("\t", $line) . "\n";
        }

        if (!empty($fileOutput)) {
            if (file_exists($file) && !is_writable($file)) {
                $generator->getLogger()->error(sprintf('Not enough permissions to write to file %s.', $file));
                return $this;
            }
            file_put_contents($file, $fileOutput);
            if (!file_exists($file)) {
                $generator->getLogger()->error(sprintf('Not enough permissions to write to file %s.', $file));
                return $this;
            }

            $linesCount = count($fileLines) > 0 ? count($fileLines) - 1 : 0;
            if ($linesCount) {
                // Save the new hash so we don't regenerate the feed if no changes were made.
                $config['hash'] = $hash;
                $feed = $this->promotionsCollection->updatePromotionsData($feed, $config);
                // Update the provider feed to take new config
                $this->provider->setFeed($feed);
                // Notify ftp uploads
                $this->provider->setFileCreated();
            }

            $generator->getLogger()->info(sprintf('Added %d lines | in %s', $linesCount, $file));
            $feed->saveMessages([
                'promotion_file' => $this->provider->getPromotionFile(),
                'promotion_added' => $linesCount
            ]);

        } else {
            $generator->getLogger()->info(sprintf('No changes | in %s', $file));
        }
        return $this;
    }

    /**
     * Creates feed header
     *
     * @return array
     */
    protected function createFeedHeader()
    {
        return array(
            'promotion_id',
            'product_applicability',
            'long_title',
            'promotion_effective_dates',
            'promotion_display_dates',
            'redemption_channel',
            'offer_type',
            'generic_redemption_code',
            'minimum_purchase_amount'
        );
    }

    /**
     * Create feed line
     *
     * @param $counter
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param array $row
     * @param \RocketWeb\ShoppingFeedsGooglePromotions\Model\Promotions\Provider\Map $map
     * @return array
     */
    protected function createFeedLine($counter, \Magento\SalesRule\Model\Rule $rule, $row = array(), $map)
    {
        return array(
            $map->mapPromotionId($counter, $rule),
            $map->mapProductApplicability($rule),
            $row['title'],
            $map->mapEffectiveDates($row),
            $map->mapDisplayDates($row),
            'ONLINE',
            $map->mapOfferType($rule),
            $map->mapGenericRedemptionCode($rule),
            $map->mapMinimumPurchaseAmount($rule)
        );
    }
}

<?xml version="1.0"?>
<!--
/**
 * Copyright © 2016 Rocket Web Inc. All rights reserved.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:noNamespaceSchemaLocation="urn:magento:module:RocketWeb_ShoppingFeeds:etc/shoppingfeeds.xsd">
    <feed name="google_shopping" translate="label,description" 
          taxonomyProvider="RocketWeb\ShoppingFeedsGoogle\Model\Taxonomy\Type\GoogleShopping">
        <label>Google Shopping</label>
        <description>Google Shopping feed type.</description>

        <directives>
            <directive name="directive_static_value">
                <label>Static Value</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\StaticValue" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\StaticValue" />
                </mappers>
            </directive>
            <directive name="directive_id">
                <label>Product Id</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductId" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Id" />
                </mappers>
                <param>0</param>
            </directive>
            <directive name="directive_url">
                <label>Product URL</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductUrl" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Url" />
                    <mapper for="configurable_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Associated\Url" />
                    <mapper for="grouped_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Associated\Url" />
                </mappers>
                <param><![CDATA[?utm_source=google_shopping]]></param>
            </directive>
            <directive name="directive_price">
                <label>Price</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductPrice" />
                <mappers filter="true">
                    <mapper for="default" type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Price" />
                    <mapper for="configurable"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Price" />
                    <mapper for="grouped"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Grouped\Price" />
                </mappers>
                <formatters>
                    <formatter type="RocketWeb\ShoppingFeeds\Model\Product\Formatter\Price\Currency" />
                </formatters>
                <param>1</param>
            </directive>
            <directive name="directive_image_link">
                <label>Product Image URL</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductImage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ImageLink" />
                </mappers>
            </directive>
            <directive name="directive_additional_image_link">
                <label>Product Additional Images URLs</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductImageAdditional" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\AdditionalImageLink" />
                </mappers>
                <param>image</param>
            </directive>
            <directive name="directive_category_image_link">
                <label>Product Category Image URL</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <param>First category found with an image is been used.</param>
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\CategoryImageLink" />
                </mappers>
            </directive>
            <directive name="directive_sale_price">
                <label>Sale Price</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductPrice" />
                <mappers filter="true">
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\SalePrice" />
                    <mapper for="configurable grouped bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\SalePrice" />
                </mappers>
                <formatters>
                    <formatter type="RocketWeb\ShoppingFeeds\Model\Product\Formatter\Price\Currency" />
                </formatters>
                <param>1</param>
            </directive>
            <directive name="directive_sale_price_effective_date">
                <label>Sale Price Date Range</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\SalePriceEffectiveDate" />
                    <mapper for="configurable grouped bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\SalePriceEffectiveDate" />
                </mappers>
                <param>Computes the start and end dates of special price. If end date is not specified, defaults to one year.</param>
            </directive>
            <directive name="directive_availability">
                <label>Availability</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers filter="true">
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Availability" />
                    <mapper for="configurable bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Availability" />
                    <mapper for="grouped"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Grouped\Availability" />
                    <mapper for="configurable_child grouped_child bundle_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Associated\Availability" />
                </mappers>
                <param><![CDATA[Computes the product availability: in stock / out of stock. See additional setting <a href="#" data-tab-id="#feed_tabs_general">Use default Stock Statuses</a>]]></param>
            </directive>
            <directive name="directive_quantity">
                <label>Inventory Count</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductQuantity" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Quantity" />
                    <mapper for="configurable grouped bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Quantity" />
                </mappers>
                <param>0</param>
            </directive>
            <directive name="directive_expiration_date">
                <label>Product Expiration in Feed</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\Expiration" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ExpirationDate" />
                </mappers>
                <param>7</param>
            </directive>
            <directive name="directive_variant_attributes" allow_output_limit="true">
                <label>Variant Attributes</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\VariantAttributes" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\VariantAttributes" />
                    <mapper for="configurable grouped bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\VariantAttributes" />
                </mappers>
            </directive>
            <directive name="directive_product_review_average">
                <label>Product Review Average</label>
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductReviewAverage" />
                </mappers>
            </directive>
            <directive name="directive_product_review_count">
                <label>Product Review Count</label>
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductReviewCount" />
                </mappers>
            </directive>
            <directive name="directive_concatenate" allow_output_limit="true">
                <label>Concatenate Attributes</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\Concatenate" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Concatenate" />
                    <mapper for="configurable_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Associated\Concatenate" />
                    <mapper for="grouped_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Grouped\Associated\Concatenate" />
                </mappers>
                <param>Product's name is "{{name}}" and sku is "{{sku}}".</param>
            </directive>
            <directive name="directive_is_bundle">
                <label>Is Bundle</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\IsBundle" />
                    <mapper for="bundle"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\IsBundle" />
                </mappers>
                <param>Outputs TRUE for bundle items and FALSE for the rest.</param>
            </directive>
            <directive name="directive_product_option" allow_output_limit="true">
                <label>Product Option</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductOption" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductOption" />
                </mappers>
            </directive>
            <directive name="directive_shipping">
                <label>Shipping</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\Shipping" />
                </mappers>
                <param><![CDATA[Please configure the section below: <a href="#" data-tab-id="#feed_tabs_shipping">Shipping</a>]]></param>
            </directive>
            <directive name="directive_shipping_weight">
                <label>Shipping Weight</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ShippingWeight" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ShippingWeight" />
                    <mapper for="configurable"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\ShippingWeight" />
                    <mapper for="bundle grouped"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Bundle\ShippingWeight" />
                </mappers>
                <param><![CDATA[kg]]></param>
            </directive>
            <directive name="directive_price_buckets">
                <label>Adwords Price Buckets</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <param><![CDATA[Please configure the <a href="#" data-tab-id="#feed_tabs_filters">Adwords Price Buckets</a> setting]]></param>
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Simple\PriceBuckets" />
                </mappers>
            </directive>
            <directive name="directive_identifier_exists">
                <label>Identifier Exists</label>
                <renderer type="RocketWeb\ShoppingFeedsGoogle\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\IdentifierExists" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Simple\IdentifierExists" />
                </mappers>
                <param><![CDATA[brand,gtin]]></param>
            </directive>
            <directive name="directive_identifier_attribute">
                <label><![CDATA[Identifier Attribute]]></label>
                <renderer type="RocketWeb\ShoppingFeedsGoogle\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\IdentifierAttribute" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Simple\IdentifierAttribute" />
                </mappers>
                <param><![CDATA[]]></param>
            </directive>
            <directive name="directive_item_group_id">
                <label>Item Group ID</label>
                <renderer type="RocketWeb\ShoppingFeedsGoogle\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ItemGroupId" />
                <param><![CDATA[sku]]></param>
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Simple\ItemGroupId" />
                    <mapper for="configurable_child grouped_child"
                            type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Configurable\Associated\ItemGroupId" />
                </mappers>
            </directive>
            <directive name="directive_product_type_magento_category" allow_output_limit="true">
                <label>Type by Magento Category Path</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\ProductTypeMagentoCategory" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductTypeMagentoCategory" />
                    <mapper for="configurable_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Configurable\Associated\ProductTypeMagentoCategory" />
                    <mapper for="grouped_child"
                            type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductTypeMagentoCategory" />
                </mappers>
                <param>3</param>
            </directive>
            <directive name="directive_product_type_by_category">
                <label>Type by Magento Category</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeeds\Model\Product\Mapper\Generic\Simple\ProductTypeByCategory" />
                </mappers>
                <param><![CDATA[Please configure the <a href="#" data-tab-id="#feed_tabs_categories">Categories Map</a>]]></param>
            </directive>
            <directive name="directive_google_category_by_category">
                <label>Taxonomy by Magento Category</label>
                <renderer type="RocketWeb\ShoppingFeeds\Block\Adminhtml\Feed\Edit\Form\Options\Renderer\HelpMessage" />
                <mappers>
                    <mapper type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Simple\GoogleCategoryByCategory" />
                    <mapper for="configurable_child"
                            type="RocketWeb\ShoppingFeedsGoogle\Model\Product\Mapper\Google\Configurable\Associated\GoogleCategoryByCategory" />
                </mappers>
                <param><![CDATA[Please configure the <a href="#" data-tab-id="#feed_tabs_categories">Categories Map</a>]]></param>
            </directive>
        </directives>

        <default_product_columns>
            <column attribute="directive_id">
                <column>id</column>
                <order>10</order>
                <param>0</param>
            </column>
            <column attribute="directive_item_group_id">
                <column>item_group_id</column>
                <order>20</order>
                <param>0</param>
            </column>
            <column attribute="name">
                <column>title</column>
                <order>30</order>
            </column>
            <column attribute="description">
                <column>description</column>
                <order>40</order>
            </column>
            <column attribute="directive_url">
                <column>link</column>
                <order>50</order>
                <param><![CDATA[?utm_source=google_shopping]]></param>
            </column>
            <column attribute="directive_image_link">
                <column>image_link</column>
                <order>60</order>
                <param>image</param>
            </column>
            <column attribute="directive_additional_image_link">
                <column>additional_image_link</column>
                <order>70</order>
                <param>image</param>
            </column>
            <column attribute="directive_price">
                <column>price</column>
                <order>80</order>
                <param>1</param>
            </column>
            <column attribute="directive_sale_price">
                <column>sale_price</column>
                <order>90</order>
                <param>1</param>
            </column>
            <column attribute="directive_sale_price_effective_date">
                <column>sale_price_effective_date</column>
                <order>100</order>
            </column>
            <column attribute="directive_availability">
                <column>availability</column>
                <order>70</order>
            </column>
            <column attribute="directive_shipping_weight">
                <column>shipping_weight</column>
                <order>120</order>
                <param>lb</param>
            </column>
            <column attribute="manufacturer">
                <column>brand</column>
                <order>130</order>
            </column>
            <column attribute="sku">
                <column>mpn</column>
                <order>140</order>
            </column>
            <column attribute="directive_static_value">
                <column>condition</column>
                <order>150</order>
                <param>new</param>
            </column>
            <column attribute="directive_product_type_magento_category">
                <column>product_type</column>
                <order>160</order>
                <param>3</param>
            </column>
            <column attribute="directive_google_category_by_category">
                <column>google_product_category</column>
                <order>170</order>
            </column>
            <column attribute="directive_identifier_exists">
                <column>identifier_exists</column>
                <order>180</order>
            </column>
            <column attribute="directive_is_bundle">
                <column>is_bundle</column>
                <order>190</order>
            </column>
            <column attribute="directive_shipping">
                <column>shipping</column>
                <order>200</order>
            </column>
            <column attribute="directive_shipping_weight">
                <column>shipping_weight</column>
                <order>210</order>
            </column>
        </default_product_columns>

        <default_feed_config>
            <general>
                <name/>
                <store_id/>
                <currency/>
                <feed_dir><![CDATA[pub/media/feeds]]></feed_dir>
                <apply_catalog_price_rules>1</apply_catalog_price_rules>
                <use_default_stock>1</use_default_stock>
                <stock_attribute_code></stock_attribute_code>
                <use_qty_increments>0</use_qty_increments>
            </general>
            <columns>
                <product_columns/>
                <exclude_attributes><![CDATA[["gallery","image","small_image","price","special_price","special_from_date","special_to_date","price_view","url_key"]]]></exclude_attributes>
            </columns>
            <categories>
                <locale>en-US</locale>
                <include_all_products>1</include_all_products>
                <provider_taxonomy_by_category/>
                <category_depth>8</category_depth>
                <taxonomy_autocomplete_enabled>1</taxonomy_autocomplete_enabled>
            </categories>
            <filters>
                <add_out_of_stock>1</add_out_of_stock>
                <product_types><![CDATA[["simple","virtual","bundle","downloadable","grouped","configurable"]]]></product_types>
                <attribute_sets/>
                <map_replace_empty_columns></map_replace_empty_columns>
                <find_and_replace/>
                <filters_output_limit>[{"column":"description","limit":"1000"},{"column":"title","limit":"70"}]</filters_output_limit>
                <skip_column_empty><![CDATA[["id","image_link","link","price"]]]></skip_column_empty>
                <skip_price_above/>
                <skip_price_below/>
                <adwords_price_buckets/>
            </filters>
            <options>
                <mode>1</mode>
                <vary_categories/>
            </options>
            <configurable>
                <associated_products_mode>1</associated_products_mode>
                <add_out_of_stock>1</add_out_of_stock>
                <inherit_parent_out_of_stock>1</inherit_parent_out_of_stock>
                <associated_products_link_add_unique>1</associated_products_link_add_unique>
                <attribute_merge_value_separator><![CDATA[, ]]></attribute_merge_value_separator>
                <configurable_map_inherit/>
            </configurable>
            <grouped>
                <associated_products_mode>1</associated_products_mode>
                <add_out_of_stock>1</add_out_of_stock>
                <associated_products_link_add_unique>1</associated_products_link_add_unique>
                <price_display_mode>0</price_display_mode>
                <grouped_map_inherit/>
            </grouped>
            <bundle>
                <associated_products_mode>1</associated_products_mode>
                <combined_weight>0</combined_weight>
            </bundle>
            <shipping>
                <methods/>
                <country/>
                <weight_column><![CDATA[shipping_weight]]></weight_column>
                <only_minimum>1</only_minimum>
                <only_free_shipping>1</only_free_shipping>
                <add_tax_to_price>0</add_tax_to_price>

                <by_region>1</by_region>
                <country_with_region/>
                <cache_enabled>1</cache_enabled>
                <ttl>168</ttl>
                <carrier_realtime><![CDATA[["ups","usps","fedex","dhl","dhlint"]]]></carrier_realtime>
            </shipping>
            <schedule>
                <!-- Leave value empty to retrieve value from config.xml default/general/ node-->
                <batch_mode></batch_mode>
                <batch_limit></batch_limit>
            </schedule>
            <ftp>
                <mode/>
                <path/>
            </ftp>
            <output_params>
                <default_value><![CDATA[]]></default_value>
                <delimiter><![CDATA[\t]]></delimiter>
                <enclose_cell><![CDATA[]]></enclose_cell>
                <enclose_escape><![CDATA[]]></enclose_escape>
            </output_params>
            <file>
                <feed><![CDATA[feed_%s.txt]]></feed>
                <log><![CDATA[log_%s.log]]></log>
            </file>
        </default_feed_config>
    </feed>
</config>

# Pepperjam Network Magento 2 Extension
## License
The license under which this software is provided is available online in PDF form at the following web address: [http://assets.pepperjam.com/legal/magento-connect-extension-eula.pdf](http://assets.pepperjam.com/legal/magento-connect-extension-eula.pdf).

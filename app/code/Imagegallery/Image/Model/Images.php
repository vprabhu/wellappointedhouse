<?php 
namespace Imagegallery\Image\Model;
class Images extends \Magento\Framework\Model\AbstractModel
{
	const CACHE_TAG = 'bfm_imagegallery_categories';
	protected function _construct()
	{
		$this->_init('Imagegallery\Image\Model\ResourceModel\Images');
	}
	
}
?>
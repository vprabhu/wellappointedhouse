<?php
namespace Imagegallery\Image\Model\ResourceModel\images;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
	{
		protected function _construct()
		{
			$this->_init('Imagegallery\Image\Model\images', 'Imagegallery\Image\Model\ResourceModel\images');
		}
	}

?>
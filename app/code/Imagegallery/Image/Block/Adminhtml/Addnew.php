<?php
namespace Imagegallery\Image\Block\Adminhtml; 
class Addnew extends \Magento\Framework\View\Element\Template
{
	public function getrecords()
    {
		$data=$this->getRequest()->getParam('id');
		if(!empty($data))
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$contactModel = $objectManager->create('Imagegallery\Image\Model\Images');
			$collection = $contactModel->getCollection()->addFieldToFilter('category_id',$data);
			return $collection->getData();
		}
		else
		{
			return "New data";
		}	
		
	}
}
	



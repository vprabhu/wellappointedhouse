<?php
namespace Imagegallery\Image\Controller\Adminhtml\Index;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultFactory;
class NewAction extends \Magento\Backend\App\Action
      {
        protected $_messageManager;
        protected $resultPageFactory;
        protected $_fileUploaderFactory;
        protected $request;
       
		public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Filesystem\DirectoryList $directory_list,\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,\Magento\Framework\Message\ManagerInterface $messageManager,\Magento\Framework\View\Result\PageFactory $resultPageFactory) 
		      {
                 parent::__construct($context);
                 $this->resultPageFactory = $resultPageFactory;
                 $this->_messageManager = $messageManager;
                 $this->_fileUploaderFactory = $fileUploaderFactory;
                 $this->directory_list = $directory_list; 
                 $this->request = $request;
               }
		
        public function execute()
        {
            if(!empty($_POST))
            {
                if(isset($_POST['category_id']))
                {
                    $data['category_id']=$_POST['category_id'];
                }

                if(!empty($_FILES["category_image"]["name"]))
                {
                    $path= $this->directory_list->getPath('media');
                    $target_dir = $path."/ImageGallery/";
                    $temp = explode(".", $_FILES["category_image"]["name"]);
                    $newfilename = round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["category_image"]["tmp_name"], $target_dir.$newfilename);
                    $data['category_image']= 'ImageGallery/'.$newfilename;
                }
                $data['title']=$_POST['title'];
                $data['description']=$_POST['description'];
                $data['sort_order']=$_POST['sort_order'];
                $contactModel = $this->_objectManager->create('Imagegallery\Image\Model\Images');
                $data_insert=$contactModel->setData($data)->save();
                if($data_insert)
                {
                    $this->_messageManager->addSuccess(__("Success"));
                    if(isset($_POST['category_id']))
                    {
                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultRedirect->setPath('image/Index/index');
                        return $resultRedirect;
                    }
                    else
                    {
                        return  $resultPage = $this->resultPageFactory->create();   
                    }
                }
                else
                {
                    $this->_messageManager->addError(__('Error'));
                    return  $resultPage = $this->resultPageFactory->create();	
                }
            }
            else
            {
                return  $resultPage = $this->resultPageFactory->create();	
            }
            
        }
    }
?>
  
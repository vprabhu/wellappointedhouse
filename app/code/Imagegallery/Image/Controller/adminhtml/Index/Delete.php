<?php
namespace Imagegallery\Image\Controller\Adminhtml\Index;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultFactory;
class Delete extends \Magento\Backend\App\Action
      {
        protected $_messageManager;
        protected $resultPageFactory;
        protected $_fileUploaderFactory;
        protected $request;
       
		public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Filesystem\DirectoryList $directory_list,\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,\Magento\Framework\Message\ManagerInterface $messageManager,\Magento\Framework\View\Result\PageFactory $resultPageFactory) 
		    {
             parent::__construct($context);
             $this->resultPageFactory = $resultPageFactory;
             $this->_messageManager = $messageManager;
             $this->_fileUploaderFactory = $fileUploaderFactory;
             $this->directory_list = $directory_list; 
             $this->request = $request;
        }
		    
        public function execute()
        {
            $delete_id=$this->getRequest()->getParam('id');
            print_r($delete_id);
            $contactModel = $this->_objectManager->create('Imagegallery\Image\Model\Images');
            $contactModel->load($delete_id);
            if($contactModel->delete())
            {
                $this->_messageManager->addSuccess(__("Success"));
            }
            else
            {
                $this->_messageManager->addError(__("error"));
            }

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('image/Index/index');
            return $resultRedirect;
            
        }
            
    }
?>
  
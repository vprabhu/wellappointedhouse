<?php
namespace Imagegallery\Image\Controller\Adminhtml\Index;
class Index extends \Magento\Backend\App\Action
      {
		protected function _isAllowed()
		{
			return $this->_authorization->isAllowed('Imagegallery_Image::wahimg_images');
		}

        protected $resultPageFactory;
		public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory) 
		{
             parent::__construct($context);
             $this->resultPageFactory = $resultPageFactory;
        }
		public function execute()
        {
			//echo "its working";
        	return  $resultPage = $this->resultPageFactory->create();	
		}
      }
 ?>
  
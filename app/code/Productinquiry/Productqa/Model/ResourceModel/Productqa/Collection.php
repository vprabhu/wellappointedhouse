<?php
namespace Productinquiry\Productqa\Model\ResourceModel\Productqa;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected function _construct()
	{
		$this->_init('Productinquiry\Productqa\Model\Productqa', 'Productinquiry\Productqa\Model\ResourceModel\Productqa');
	}
}

?>
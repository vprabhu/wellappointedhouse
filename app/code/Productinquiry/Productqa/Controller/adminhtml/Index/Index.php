<?php
namespace Productinquiry\Productqa\Controller\Adminhtml\Index;
class Index extends \Magento\Backend\App\Action
      {
		protected function _isAllowed()
		{
			return $this->_authorization->isAllowed('Productinquiry_Productqa::greetings_productqa');
		}

        protected $resultPageFactory;
        public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory
        ) {
             parent::__construct($context);
             $this->resultPageFactory = $resultPageFactory;
        }
         public function execute()
        {
			if(!empty($_POST))
			{
				foreach($_POST['items'] as $key=>$value)
				{
					$data['name']=$value['name'];
					$data['email']=$value['email'];
					$data['question']=$value['question'];
					
					$data['product_sku']=$value['product_sku'];
					$data['answer']=$value['answer'];
					$data['productqa_id']=$value['productqa_id'];
					

					if(!empty($value['answer']))
					{
						$data['status']=2;
						$data['status_name']="Disable";
						$to = $value['email'];
						$subject = "Customer Service";
						$txt = "<div>Hello ".$value['name']."</div>";
						$txt .= "<div>Thank you for your interest in</div>";
						$txt .= "<div>Here is the info might be helpful to you:</div>";
						$txt .= "<div>".$value['answer']."</div>";
						$headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= " From: no-reply@wah.underdev.in" . "\r\n";
						mail($to,$subject,$txt,$headers);
							
					}
					
				}
			
        
			$contactModel = $this->_objectManager->create('Productinquiry\Productqa\Model\Productqa');
			$data_insert=$contactModel->setData($data)->save();
			
			echo json_encode("sucess");
			}
			else
			{
				return  $resultPage = $this->resultPageFactory->create();	
			}
			
        }
      }
    ?>
  
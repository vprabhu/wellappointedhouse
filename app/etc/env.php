<?php
return [
  'backend' => [
    'frontName' => 'admin'
  ],
  'crypt' => [
    'key' => '941b897a4fbef5477f0f727a8eea48a1'
  ],
  'db' => [
    'table_prefix' => '',
    'connection' => [
      'default' => [
        'host' => '10.210.196.240',
        'dbname' => 'staging_wah',
        'username' => 'root_user',
        'password' => 'RUvn53eGu8QYzrET',
        'active' => '1'
      ]
    ]
  ],
  'resource' => [
    'default_setup' => [
      'connection' => 'default'
    ]
  ],
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'production',
  'session' => [
    'save' => 'files'
  ],
  'cache_types' => [
    'config' => 0,
    'layout' => 0,
    'block_html' => 0,
    'collections' => 0,
    'reflection' => 0,
    'db_ddl' => 0,
    'eav' => 0,
    'customer_notification' => 0,
    'config_integration' => 0,
    'config_integration_api' => 0,
    'full_page' => 0,
    'translate' => 0,
    'config_webservice' => 0,
    'vertex' => 0
  ],
  'install' => [
    'date' => 'Fri, 28 Sep 2018 09:07:42 +0000'
  ],
  'system' => [
    'default' => [
      'dev' => [
        'debug' => [
          'debug_logging' => '0'
        ]
      ]
    ]
  ]
];
